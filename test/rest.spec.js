const chai = require('chai');
const { it } = require('mocha');
const expect = chai.expect;
const request = require('request');
const app = require('../src/server');
const port = 3000;

let server;

// async request
const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (err, response) => {
        if (err) reject(err)
        else resolve(response)
    });
});

describe('Test REST API', () => {
    beforeEach('Start server', async () => {
        server = app.listen(port);
    });

    describe('Test functionality', () => {
        it('GET /add?a=1&b=2 returns 3', async () => {
            const options = {
                method: 'GET',
                url: 'http://localhost:3000/add',
                qs: { a: '1', b: '2' }
            };
            await arequest(options).then((response) => {
                console.log({message: response.body});
                expect(response.body).to.equal('3');
            }).catch((err) => {
                console.log({error: err});
                expect(true).to.equal(false, 'add function failed');
            });
        })
    });

    afterEach(() => {
        server.close();
    });

})
